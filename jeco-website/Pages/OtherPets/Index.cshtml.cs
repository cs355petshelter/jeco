﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using jeco_website.Data;

namespace jeco_website.Pages.OtherPets
{
    public class IndexModel : PageModel
    {
        private readonly jeco_website.Data.ApplicationDbContext _context;

        public IndexModel(jeco_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Pet> Pet { get;set; }

        public async Task OnGetAsync()
        {
            Pet = await _context.Pets.ToListAsync();
        }
    }
}
