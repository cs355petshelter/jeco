﻿using jeco_website.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jeco_website.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly jeco_website.Data.ApplicationDbContext _context;

        public IndexModel(ILogger<IndexModel> logger, jeco_website.Data.ApplicationDbContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task OnGetAsync()
        {
            Pet = await _context.Pets.ToListAsync();


        }

        [BindProperty]
        public IList<Pet> Pet { get; set; }
    }
}
