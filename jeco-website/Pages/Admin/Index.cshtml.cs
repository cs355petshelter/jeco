﻿using jeco_website.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jeco_website.Pages.Admin
{
    [Authorize(Roles = "ADMIN")]
    public class IndexModel : PageModel
    {

        private readonly jeco_website.Data.ApplicationDbContext _context;

        public IndexModel(jeco_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }
    }
}
