﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using jeco_website.Data;

namespace jeco_website.Pages.Inquiries
{
    public class DetailsModel : PageModel
    {
        private readonly jeco_website.Data.ApplicationDbContext _context;

        public DetailsModel(jeco_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public Inquiry Inquiry { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Inquiry = await _context.Inquiries.FirstOrDefaultAsync(m => m.Id == id);

            if (Inquiry == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
