﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using jeco_website.Data;
using Microsoft.AspNetCore.Authorization;

namespace jeco_website.Pages.Inquiries
{
    [Authorize(Roles = "ADMIN")]
    public class IndexModel : PageModel
    {
        private readonly jeco_website.Data.ApplicationDbContext _context;

        public IndexModel(jeco_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Inquiry> Inquiry { get;set; }

        public async Task OnGetAsync()
        {
            Inquiry = await _context.Inquiries.ToListAsync();
        }
    }
}
