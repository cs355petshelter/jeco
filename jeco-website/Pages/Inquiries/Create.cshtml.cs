﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using jeco_website.Data;
using Microsoft.EntityFrameworkCore;

namespace jeco_website.Pages.Inquiries
{
    public class CreateModel : PageModel
    {
        private readonly jeco_website.Data.ApplicationDbContext _context;

        public CreateModel(jeco_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Pet Pet { get; set; }

        public async Task<IActionResult> OnGetAsync(int ? id)
        {
            Pet = await _context.Pets.FirstOrDefaultAsync(M => M.Id == id);
            return Page();
        }

        [BindProperty]
        public Inquiry Inquiry { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Inquiries.Add(Inquiry);
            await _context.SaveChangesAsync();

            return RedirectToPage("../Index");
        }
    }
}
