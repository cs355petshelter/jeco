﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using jeco_website.Data;
using System.Diagnostics;
using System.IO;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace jeco_website.Pages.Pets
{
    [Authorize(Roles="ADMIN")]
    public class EditModel : PageModel
    {
        private readonly jeco_website.Data.ApplicationDbContext _context;
        private readonly IHostEnvironment _environment;

        [BindProperty]
        public Pet Pet { get; set; }
        public IFormFile PictureUpload { get; set; }

        public EditModel(jeco_website.Data.ApplicationDbContext context, IHostEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Pet = await _context.Pets.FirstOrDefaultAsync(m => m.Id == id);

            if (Pet == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {

            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Pet).State = EntityState.Modified;

            // Generate Unique Filename
            string fileName = PictureUpload.FileName;
            string extension = Path.GetExtension(fileName);
            string newFileName = Path.GetRandomFileName() + extension;

            // Save File to Dir
            string target = $"{_environment.ContentRootPath}/wwwroot/img/Pets/{newFileName}";
            using (var stream = new FileStream(target, FileMode.Create))
            {
                await PictureUpload.CopyToAsync(stream);
            }

            Debug.WriteLine(newFileName);

            // Store Unique Name in DB
            Pet.Filename = newFileName;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PetExists(Pet.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            // Save uploaded file
            if (PictureUpload == null || PictureUpload.Length == 0)
            {
                return NotFound();
            }
            return RedirectToPage("./Index");
        }

        private bool PetExists(int id)
        {
            return _context.Pets.Any(e => e.Id == id);
        }
    }
}
