﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using jeco_website.Data;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace jeco_website.Pages.Pets
{
    public class IndexModel : PageModel
    {
        private readonly jeco_website.Data.ApplicationDbContext _context;

        public IndexModel(jeco_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IList<Pet> Pet { get;set; }
        [BindProperty(SupportsGet = true)]
        public string SearchString { get; set; }
        public SelectList ListedPets { get; set; }
        [BindProperty(SupportsGet = true)]
        public string ListedPet { get; set; }

        public async Task OnGetAsync()
        {
            var pets = from p in _context.Pets select p;
            if (!string.IsNullOrEmpty(SearchString))
            {
                pets = pets.Where(s => s.Name.Contains(SearchString));
            }
            Pet = await pets.ToListAsync();
        }
    }
}
