﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using jeco_website.Data;
using Microsoft.AspNetCore.Authorization;

namespace jeco_website.Pages.Pets
{
    [Authorize(Roles="ADMIN")]
    public class CreateModel : PageModel
    {
        private readonly jeco_website.Data.ApplicationDbContext _context;

        public CreateModel(jeco_website.Data.ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            return Page();
        }

        [BindProperty]
        public Pet Pet { get; set; }

        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Pets.Add(Pet);

            // Set added pet date to the current time
            Pet.CreatedAt = DateTime.Now;

            // Determine pet category from species
            if (Pet.Species == "Dog")
            {
                Pet.Category = "Dogs";
            }
            else if (Pet.Species == "Cat") 
            {
                Pet.Category = "Cats";
            }
            else
            {
                Pet.Category = "OtherPets";
            }

            await _context.SaveChangesAsync();


            return RedirectToPage("./Index");
        }
    }
}
