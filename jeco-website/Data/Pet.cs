﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jeco_website.Data
{
    public class Pet
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Species { get; set; }
        public string Breed { get; set; }
        public DateTime Birthdate { get; set; }
        public int Sex { get; set; }
        public string Filename { get; set; }
        public IdentityUser UserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Category { get; set; }

    }
}