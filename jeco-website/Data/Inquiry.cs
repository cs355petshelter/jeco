﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace jeco_website.Data
{
    public class Inquiry
    {
        public int Id { get; set; }
        public IdentityUser InquirerId { get; set; }
        public IdentityUser OwnerId { get; set; }
        public Pet Pet { get; set; }
        public DateTime CreatedAt { get; set; }
        public String Status { get; set; }
        public String PetName { get; set; }

    }
}
