﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace jeco_website.Migrations
{
    public partial class Ellie : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PetName",
                table: "Inquiries",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PetName",
                table: "Inquiries");
        }
    }
}
